#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
offboard_ctrl.py: Controlling the setpoints

"""

###############################################
# Standard Imports                            #
###############################################
import time
import threading
import math
import numpy as np

import csv
import os

from scipy.spatial import distance

from shapely.geometry import Polygon, Point, LinearRing

###############################################
# ROS Imports                                 #
###############################################
import rospy
import rospkg

###############################################
# ROS Topic messages                          #
###############################################
from std_msgs.msg import String
from std_msgs.msg import Float32

from geometry_msgs.msg import PoseStamped

from sensor_msgs.msg import NavSatFix

from mavros_msgs.msg import State
from mavros_msgs.msg import RCOut


###############################################
# ROS Service messages                        #
###############################################
from mavros_msgs.srv import CommandBool, SetMode, CommandTOL
from std_srvs.srv import Empty, EmptyRequest, EmptyResponse

###############################################
# Offboad Control class                       #
###############################################
class OffboardControl:
    def __init__(self, *args):
        self.current_state = State()
        self.current_local_pose = PoseStamped()
        self.current_rc_out = RCOut()

        rospy.init_node('offboard_ctrl')

        self.offb_set_mode = SetMode

        self.prev_state = self.current_state

        # Mahalanobis Distance
        self.rospack = rospkg.RosPack()
        self.path = self.rospack.get_path('eit_playground')

        print("Path: {}".format(self.path))

        self.iv = np.identity(4)
        self.mean_vals = [0, 0, 0, 0]

        # Path Square
        self.poly = Polygon([(0.5, 0.5), (0.5, -0.5), (-0.5, -0.5), (-0.5, 0.5)])

        # Path (cirle)
        self.target_x = 0
        self.target_y = 0
        self.target_z = 1

        self.radius = 1

        self.geometry = ""

        self.mahal_dist = Float32()

        # Publishers
        self.local_pos_pub = rospy.Publisher('/mavros/setpoint_position/local', PoseStamped, queue_size=10)
        self.mahal_dist_pub = rospy.Publisher('/mahalanobis/dist', Float32, queue_size=10)

        # Subscribers
        self.state_sub = rospy.Subscriber('/mavros/state', State, self.cb_state)
        self.sub_target = rospy.Subscriber('/mavros/offbctrl/target', PoseStamped, self.cb_target)

        self.state_sub = rospy.Subscriber('/mavros/local_position/pose', PoseStamped, self.cb_local_pose, queue_size = 1)
        self.state_sub = rospy.Subscriber('/mavros/rc/out', RCOut, self.cb_rc_out, queue_size = 1)

        # Services
        self.arming_client = rospy.ServiceProxy('/mavros/cmd/arming', CommandBool)
        self.takeoff_client = rospy.ServiceProxy('/mavros/cmd/takeoff', CommandTOL)
        self.set_mode_client = rospy.ServiceProxy('/mavros/set_mode', SetMode)

        ## Create services
        self.setpoint_controller_server()

        # Init msgs
        self.target = PoseStamped()
        self.target.pose.position.x = 0
        self.target.pose.position.y = 0
        self.target.pose.position.z = 1

        self.last_request = rospy.get_rostime()
        self.state = "INIT"

        self.rate = rospy.Rate(20.0) # MUST be more then 2Hz

        self.t_run = threading.Thread(target=self.navigate)
        self.t_run.start()
        print(">> SetPoint controller is running (Thread)")

        # Spin until the node is stopped

        #time.sleep(5)
        #tmp = Empty()
        #self.switch2offboard(tmp)

        rospy.spin()

    """
    Callbacks
    * cb_state
    * cb_target
    """
    def cb_state(self,state):
        self.current_state = state

    def cb_target(self,data):
        self.set_target(data)

    def cb_local_pose(self,data):
        self.current_local_pose = data

        #print(self.current_local_pose)

    def cb_rc_out(self,data):
        self.current_rc_out = data

        #print(self.current_rc_out)

    """
    Services
    * s_arm:
    * s_stop:
    * s_s2o:
    * s_s2l:
    * s_circle:
    * s_square:
    """
    def setpoint_controller_server(self):
        s_arm = rospy.Service('setpoint_controller/arm', Empty, self.arm)
        s_stop = rospy.Service('setpoint_controller/stop', Empty, self.stop)
        s_s2o = rospy.Service('setpoint_controller/switch2offboard', Empty, self.switch2offboard)
        s_s2l = rospy.Service('setpoint_controller/switch2land', Empty, self.switch2land)
        s_circle = rospy.Service('setpoint_controller/circle', Empty, self.start_circle)
        s_square = rospy.Service('setpoint_controller/square', Empty, self.start_square)
        s_afdi = rospy.Service('setpoint_controller/afdi', Empty, self.start_afdi)

        print("The SetPoint Controller is ready")


    """
    Mahalanobis Distance
    * quaternion_to_euler:
    * s_stop:
    """
    # Quaternion and Euler conversion(s)
    def quaternion_to_euler(self, x=None, y=None, z=None, w=None):
        t0 = +2.0 * (w * x + y * z)
        t1 = +1.0 - 2.0 * (x * x + y * y)

        roll = math.atan2(t0, t1)

        t2 = +2.0 * (w * y - z * x)
        t2 = +1.0 if t2 > +1.0 else t2
        t2 = -1.0 if t2 < -1.0 else t2

        pitch = math.asin(t2)

        t3 = +2.0 * (w * z + x * y)
        t4 = +1.0 - 2.0 * (y * y + z * z)

        yaw = math.atan2(t3, t4)

        #print("RPY: {} {} {}".format(roll,pitch,yaw))
        return [roll,pitch,yaw]

    def mahalanobis(self, x=None, data=None, cov=None):
        """Compute the Mahalanobis Distance between each row of x and the data
        x    : vector or matrix of data with, say, p columns.
        data : ndarray of the distribution from which Mahalanobis distance of each observation of x is to be computed.
        cov  : covariance matrix (p x p) of the distribution. If None, will be computed from data.
        """

        x_minus_mu = x - np.mean(data)
        if not cov:
            cov = np.cov(data.values.T)

        inv_covmat = np.linalg.inv(cov)
        left_term = np.dot(x_minus_mu, inv_covmat)
        mahal = np.dot(left_term, x_minus_mu.T)

        return mahal.diagonal()

    """
    State
    * set_state:
    * get_state:
    """
    def set_state(self, data):
        self.state = data
        print("New State: {}".format(data))

    def get_state(self):
        return self.state

    """
    Target position
    * set_target:
    """
    def set_target(self, data):
        self.target = data

    def set_target_xyz(self,x,y,z,delay):

        #if(delay > 0.1):
        #    print(">> New setpoint: {} {} {}".format(x,y,z))

        self.target.pose.position.x = x
        self.target.pose.position.y = y
        self.target.pose.position.z = z

        time.sleep(delay)

    def get_target(self):
        return self.target

    """
    Commands
    * switch2offboard:
    * arm:
    * navigate:
    * start_circle:
    * cicle:
    * stop:
    """
    def switch2offboard(self,r):
        print(">> Starting OFFBOARD mode")

        last_request = rospy.get_rostime()
        while self.current_state.mode != "OFFBOARD":
            now = rospy.get_rostime()
            if(now - last_request > rospy.Duration(5)):
                print("Trying: OFFBOARD mode")
                self.set_mode_client(base_mode=0, custom_mode="OFFBOARD")
                last_request = now

        tmp = Empty()
        self.arm(tmp)

        return {}

    def arm(self,r):
        print(">> Arming...")
        last_request = rospy.get_rostime()
        while not self.current_state.armed:
            now = rospy.get_rostime()
            if(now - last_request > rospy.Duration(5.)):
               self.arming_client(True)
               last_request = now

        return {}

    def switch2land(self,r):
        print(">> Activating LAND mode")

        last_request = rospy.get_rostime()
        while self.current_state.mode != "AUTO.LAND":
            now = rospy.get_rostime()
            if(now - last_request > rospy.Duration(5)):
                print("Trying: LAND mode")
                self.set_mode_client(base_mode=0, custom_mode="AUTO.LAND")
                last_request = now

        print("Landing...")

        return {}


    def navigate(self):
        self.set_state("RUNNING")
        while not rospy.is_shutdown():
            self.target.header.frame_id = "base_footprint"
            self.target.header.stamp = rospy.Time.now()

            self.local_pos_pub.publish(self.target)
            self.rate.sleep()

        print(">> Navigation thread has stopped...")
        self.set_state("STOP")

    def start_circle(self, r):
        self.t_circle = threading.Thread(target=self.circle)
        self.t_circle.start()
        print(">> Starting circle (Thread)")

        return {}

    def circle(self):
        sides = 360
        radius = 1
        i = 0
        delay = 0.1

        if(self.state != "RUNNING"):
            print(">> SetPoint controller is not running...")
        else:
            self.set_state("CIRCLE")

            while self.state == "CIRCLE":
                x = radius * math.cos(i*2*math.pi/sides)
                y = radius * math.sin(i*2*math.pi/sides)
                z = self.target.pose.position.z

                self.set_target_xyz(x,y,z,delay)

                i = i + 1

                # Reset counter
                if(i > 360):
                    i = 0

            self.set_state("RUNNING")


    def start_square(self, r):
        self.t_square = threading.Thread(target=self.square)
        self.t_square.start()
        print(">> Starting square (Thread)")

        return {}

    def square(self):
        delay = 5
        x_sqaure = [-0.5,-0.5,0.5,0.5]
        y_sqaure = [0.5,-0.5,-0.5,0.5]
        i = 0

        if(self.state != "RUNNING"):
            print(">> SetPoint controller is not running...")
        else:
            self.set_state("SQUARE")

            while self.state == "SQUARE":
                x = x_sqaure[i]
                y = y_sqaure[i]
                z = self.target.pose.position.z

                self.set_target_xyz(x,y,z,delay)

                i = i + 1

                # Reset counter
                if(i > 3):
                    i = 0

            self.set_state("RUNNING")


    def start_afdi(self, r):
        self.t_afdi = threading.Thread(target=self.afdi)
        self.t_afdi.start()
        print(">> Starting AFDI (Thread)")

        return {}

    def afdi(self):
        print(">> Starting AFDI")

        if self.state == "CIRCLE":
            self.geometry = "circle"
        elif self.state == "SQUARE":
            self.geometry = "square"
        else:
            self.geometry = "hover"

        with open(self.path+'/mahal/'+self.geometry+'/errs_mahala.csv', 'r') as csvFile:
            reader = csv.reader(csvFile)
            next(reader)

            print("Geometry: {}".format(self.geometry))

            for row in reader:
                print(row)
                self.mean_vals[0] = float(row[0])
                self.mean_vals[1] = float(row[1])
                self.mean_vals[2] = float(row[2])
                self.mean_vals[3] = float(row[3])
            csvFile.close()

        print("Mahalanobis Distance means: {}".format(self.mean_vals))

        while self.state == "RUNNING" or self.state == "CIRCLE" or self.state == "SQUARE":
            rpy = self.quaternion_to_euler(
                self.current_local_pose.pose.orientation.x,
                self.current_local_pose.pose.orientation.y,
                self.current_local_pose.pose.orientation.z,
                self.current_local_pose.pose.orientation.w)

            #print("Ch: {} {} {} {} {} {}  --> Pose {} {} {}  {} {} {}".format(
            #    self.current_rc_out.channels[0],
            #    self.current_rc_out.channels[1],
            #    self.current_rc_out.channels[2],
            #    self.current_rc_out.channels[3],
            #    self.current_rc_out.channels[4],
            #    self.current_rc_out.channels[5],
            #    self.current_local_pose.pose.position.x,
            #    self.current_local_pose.pose.position.y,
            #    self.current_local_pose.pose.position.z,
            #    rpy[0],
            #    rpy[1],
            #    rpy[2]))

            errs = 0.0

            xc = self.current_local_pose.pose.position.x
            yc = self.current_local_pose.pose.position.y
            zc = self.current_local_pose.pose.position.z

            if self.geometry == "circle":
                #print("Geometry (check): {}".format(self.geometry))
                p2xy = math.fabs(math.sqrt(xc*xc+yc*yc)-self.radius) #math.fabs(math.sqrt(float(row[4])+float(row[5])-radius))
                p2z = zc-self.target_z
                errs = math.sqrt(p2xy*p2xy+p2z*p2z)
            elif self.geometry == "square":
                point = Point(xc, yc)
                pol_ext = LinearRing(self.poly.exterior.coords)

                d = pol_ext.project(point)
                p = pol_ext.interpolate(d)

                closest_point_coords = list(p.coords)[0]

                p2x = closest_point_coords[0]-xc
                p2y = closest_point_coords[1]-yc
                p2z = zc-self.target_z

                errs = math.fabs(math.sqrt(p2x*p2x+p2y*p2y+p2z*p2z))
            else:
                #print("Geometry (check): {}".format(self.geometry))
                p2x = xc
                p2y = yc
                p2z = zc-self.target_z
                errs = math.fabs(math.sqrt(p2x*p2x+p2y*p2y+p2z*p2z))

            self.mahal_dist.data = distance.mahalanobis([errs, math.fabs(rpy[0]), math.fabs(rpy[1]), math.fabs(rpy[2])], self.mean_vals, self.iv)
            #print("Mahalanobis Distance: {} ".format(self.mahal_dist.data))

            self.mahal_dist_pub.publish(self.mahal_dist)

            if self.mahal_dist.data > 0.30:
                print(">> Thresh detected: {}".format(self.mahal_dist))
                self.state = "LANDING"
                print(">> Landing...")
                tmp = Empty()
                self.switch2land(tmp)

            time.sleep(0.1)

    """
    Stop
    """
    def stop(self,r):
        self.set_target_xyz(0,0,1,0.5)
        self.set_state("STOP")
        return {}

if __name__ == '__main__':
    SPC = OffboardControl()
